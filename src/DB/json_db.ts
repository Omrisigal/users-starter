import fs from 'fs/promises';


export async function init(filename:string){
    try{
        await fs.readFile('src/output/'+filename, 'utf8');

     } catch (err) {
      await fs.writeFile('src/output/'+filename, JSON.stringify([],null,2));
     }
}

export async function getData(filename: string):Promise<any[]>{
    const data_json = await fs.readFile('src/output/'+filename, 'utf8');
    let data = JSON.parse(data_json);
    return data; 
}

export async function save(filename: string, Objects: any){
    await fs.writeFile('src/output/'+filename, JSON.stringify(Objects, null, 2));
}