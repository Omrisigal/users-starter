
import express, {Response, Request, NextFunction} from 'express';
import {user} from '../logic/users/usersService.js';
import {myError} from '../interfaces/error_messages.js';
import {json_db_filename as db, log_filename as log_file} from '../constans.js';
import {logMiddleware as log} from '../middleware/logger.js';
import * as JSON_DB from '../DB/json_db.js';
import * as TEST from '../middleware/checkParams.js';
import * as UserService from '../logic/users/usersService.js';
import userModel from '../logic/users/UserModel.js';

class validate_message{
    constructor(public message = 'User', public status = 200 ){}
}

const router = express.Router();

async function init(req : Request,res: Response,next: NextFunction){
    req.id = Math.random().toString(36).substring(7);
    try{
        await JSON_DB.init(db);
        next();
    }
    catch(error){
        next(new myError('DB init error'));
    }
}

router.use(init);
router.use(log);

function checkUserParams(required_params: string[]){
    return function(req : Request,res: Response,next: NextFunction){
        try{
            TEST.checkParams(required_params, [req.body.name, req.body.age]);
            next();
        }
        catch(error){
            next(error);
        }
    }
};


//create user
router.post('/create',checkUserParams(['name','age']), async (req : Request,res: Response,next: NextFunction) => {
    try{
        let user = await UserService.create(req?.body);
        let message = new validate_message('User Created: ');
        message.message += JSON.stringify(user);
        res.status(message.status).send(message);
        
    }
    catch(error){
        next(error);
    }
    
})

//update user
router.post('/update/:id', checkUserParams(['name','age']),async (req : Request,res: Response,next: NextFunction) => {
    let id = req.params.id;
    let  update_user :user = {
        "id" : id,
        "name": req.body.name,
        "age" : req.body.age
    }
    let message = new validate_message('User Updated: ');
    try{
        let user = await UserService.update(id, update_user);
        message.message += JSON.stringify(user);
        res.status(message.status).send(message);
    }
    catch(error) {
        next(error);
    }

})

//delete user by id
router.delete('/delete/:id', async (req : Request,res: Response,next: NextFunction) => {
    let id = req.params.id;
    let message = new validate_message('User Deleted: ');
    try{
        let user = await UserService.delete_user(id);
        message.message+= JSON.stringify(user);
        res.status(message.status).send(message);
    }
    catch(error) {
        next(error);
    }

})

//show user
router.get('/:id', async (req : Request,res: Response,next: NextFunction) => {
    let id = req.params.id;
    let message = new validate_message('User read: ');
    try{
        let user = await userModel.get(id);
        res.status(message.status).send(user);
    }
    catch(error) {
        next(error);
    }

})

//show all users
router.get('/', async (req : Request,res: Response,next: NextFunction) => {
    let message = new validate_message('Users read: ');
    try{
        let users = await userModel.getAll();
        res.status(message.status).send(users);
    }
    catch(error){
        next(error);
    }
})

export default router;