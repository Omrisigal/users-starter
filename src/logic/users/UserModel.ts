import * as JSON_DB from '../../DB/json_db.js';
import { json_db_filename as db_filename } from '../../constans.js';
import { user } from './usersService.js';

class UserModel{
    async get(id? :string) {
        let  users = await this.getAll();
        return users.find((item)=> item.id === id);
    };
    async getAll(){
        return await JSON_DB.getData(db_filename);

    };
    async save(users : user[]){
        JSON_DB.save(db_filename, users);
    };

}

let userModel = new UserModel();
export default userModel;