

import * as JSON_DB from '../../DB/json_db.js';
// import {user} from '../../interfaces/user.js'
import {BadInputError, UrlError} from '../../interfaces/error_messages.js'
import userModel from './UserModel.js';

export interface user {
    id: string,
    name: string,
    age: number;
}
export type userDetails = Omit<user, "id">;

const uid = () => Math.random().toString(36).substring(6);


export async function create(data: any) {
    let user: user = {
        id: uid(),
        name: data.name,
        age: data.age
    }
    let users = await userModel.getAll();
    users.push(user);
    userModel.save(users);
    return user;
}

export async function update(id: string, user : user){
    let old_user = await userModel.get(id);
    if(!old_user){
        throw (new UrlError(id));
    }else{
        let users = await userModel.getAll();
        let index = users.findIndex((item)=> item.id === old_user.id);
        users[index] = user;
        userModel.save(users);
        return user;
    }
    
}

export async function delete_user(id: string){
    let users = await userModel.getAll();
    let foundIndex = users.findIndex(item => item.id == id);
    if(foundIndex == -1) {
        throw (new UrlError(id));
        }
    else{
        users.splice(foundIndex,1);
        userModel.save(users);
        }
    return id;
}

