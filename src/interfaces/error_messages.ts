

export class myError extends Error{
    status: number;
    constructor(message:string, status?: number){
        super(message);
        this.status = status? status : 500;
       
    }
}

export class UrlError extends myError{
    status = 404;
    constructor(message:string, stack?:string){
        super('Bad Url input: '+message);
        if(stack){
            this.stack = stack;
        }
    }
}

export class BadInputError extends myError{
    status = 400;
    constructor(message:string, stack?:string){
        super('Bad input: '+message);
        if(stack){
            this.stack = stack;
        }
    }
}