import log from "@ajar/marker";
import express, {Response, Request, NextFunction} from 'express';
import morgan from 'morgan';
import usersRouter from "./Routers/users.js";
import { errorHandler } from "./middleware/errors.js";


const { PORT = 3030, HOST = "localhost" } = process.env;

const app = express()

app.use(morgan('dev') ); 
app.use(express.json());
app.use('/users',usersRouter);
app.use(errorHandler);

app.use('*',  (req : Request,res: Response,next: NextFunction) => {
    const full_url = new URL(req.url, `http://${req.headers.host}`);
    next({
      "message":` - 404 - url ${full_url.href+full_url.pathname} was not found BY OMRI`,
      "status": 404
    })
    res.status(404).json(` - 404 - url ${full_url.href+full_url.pathname} was not found BY OMRI`)
})

app.listen(Number(PORT), HOST as string , ()=> {
        log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
    });