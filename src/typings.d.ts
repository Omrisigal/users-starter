// import { User } from "./db/users.model.js";

export declare interface User{
  id: string;
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
}

declare global {

namespace Express {
  interface Request {
      id: string;
  }
}
}
