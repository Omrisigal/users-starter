
import express, {Response, Request, NextFunction} from 'express';
import { myError } from '../interfaces/error_messages.js';
import fs from 'fs';
import { log_filename as log_file } from '../constans.js';

export async function logMiddleware(req : Request,res: Response,next: NextFunction){
    const stream = fs.createWriteStream('src/output/'+log_file, {encoding:'utf-8', flags:"a"});
    const method = req.method
    const path = req.baseUrl + req.url;
    const timenow = Date.now();
    const line = method + ' ' + path + ' ' + timenow + '\n';
    try{
        fs.appendFile('src/output/'+log_file, line, function (err) {
            if (err) throw err;
        });
        next();
    }
    catch(error){
        next(new myError('Log writing error'));
    }
}