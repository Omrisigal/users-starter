

import { myError } from '../interfaces/error_messages.js';

export function checkParams(required : string[], sent_arr: string[]) : void {
    if(sent_arr.length != required.length) {
            throw new myError(`param number does not equal to ${required.length}`,400)

    }
    else{
        for (const field of sent_arr) {
            if(!field){
                throw new myError(`field ${field} is missing`,400)
            }
        }
    }
}

