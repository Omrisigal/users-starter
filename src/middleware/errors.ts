import express, {Response, Request, NextFunction} from 'express';
import { myError } from '../interfaces/error_messages.js';
import fs from 'fs';
import { errors_log } from '../constans.js';

export async function errorHandler (err: myError, req : Request,res: Response,next: NextFunction) {
    const stream = fs.createWriteStream('src/output/'+errors_log, {encoding:'utf-8', flags:"a"});
    let status = err.status || 500;
    let line = 'request id: '+ req.id + ' , error status: '+status+' , error message: ' + err.message + ' '+ ', stacktrace :'+ err.stack+ '\n';
    fs.appendFile('src/output/'+ errors_log, line, function (error) {
        if (error) throw error;
      });
    console.log('this is the error IN ERROR HANDLER: ',err.message);
    res.status(status).send(err.message);
  }